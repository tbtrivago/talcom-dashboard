job "tc-dashboard" {
  # datacenters = ["dus"]
  datacenters = ["campus"]
  # region = "europe"
  region = "DEV"

  type = "service"

  update {
    # The "stagger" parameter specifies to do rolling updates of this job every
    # 10 seconds.
    stagger = "10s"

    # The "max_parallel" parameter specifies the maximum number of updates to
    # perform in parallel. In this case, this specifies to update a single task
    # at a time.
    max_parallel = 1
  }

  group "tc-dashboard" {
    # The "count" parameter specifies the number of the task groups that should
    # be running under this group. This value must be non-negative and defaults
    # to 1.
    count = 1

    # The "restart" stanza configures a group's behavior on task failure. If
    # left unspecified, a default restart policy is used based on the job type.
    #
    # For more information and examples on the "restart" stanza, please see
    # the online documentation at:
    #
    #     https://www.nomadproject.io/docs/job-specification/restart.html
    #
    restart {
      # The number of attempts to run the job within the specified interval.
      attempts = 10
      interval = "5m"

      # The "delay" parameter specifies the duration to wait before restarting
      # a task after it has failed.
      delay = "25s"

      # The "mode" parameter controls what happens when a task has restarted
      # "attempts" times within the interval. "delay" mode delays the next
      # restart until the next interval. "fail" mode does not restart the task
      # if "attempts" has been hit within the interval.
      mode = "fail"
    }

    task "tc-dashboard" {
      # The "driver" parameter specifies the task driver that should be used to
      # run the task.
      driver = "docker"

      # The "config" stanza specifies the driver configuration, which is passed
      # directly to the driver to start the task. The details of configurations
      # are specific to each driver, so please see specific driver
      # documentation for more information.
      config {
        image = "trivago/tc-dashboard"
        port_map {
          http = 3000
        }

        auth {
          username = "username"
          password = "password"
        }

        force_pull = true
      }

      resources {
        cpu    = 100
        memory = 512
        network {
          mbits = 10
          port "http" {}
        }
      }

#      service {
#        name = "docker-images"
#        tags = ["trv-env-prod", "trv-net-prod-internal"]
#        port = "http"
#        check {
#          name     = "alive"
#          type     = "tcp"
#          interval = "10s"
#          timeout  = "2s"
#        }
#      }

      service {
        tags = ["trv-env-dev", "trv-net-dev-internal"]
        name = "tc-dashboard"
        port = "http"

        check {
          type     = "tcp"
          name     = "http"
          interval = "5s"
          timeout  = "2s"
        }
      }
    }
  }
}