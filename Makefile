.PHONY: build
build:
	docker-compose build

.PHONY: push
push: build
	docker-compose push

.PHONY: deploy
deploy:
	nomad stop tc-dashboard && nomad run nomad.hcl

.PHONY: run
run:
	docker-compose run