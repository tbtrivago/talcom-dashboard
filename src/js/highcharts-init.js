export const initHighCharts = () => {
    Highcharts.dateFormat('%m Day: %d Year: %Y', 20, false);
    Highcharts.setOptions({
        global: {
            useUTC: true,
        },
    });
};
