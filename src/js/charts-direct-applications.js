import * as shared from './shared';

document.addEventListener('DOMContentLoaded', function(event) {
    var tableBody = document.getElementById('table-body'),
        LOCAL_STORAGE_ID_JOBIDS = 'direct-app-jobids';

    function updateJobRequisitionData(data) {
        tableBody.innerHTML = getTableMarkup(data);
    }

    function setTextArea(jobIDs) {
        try {
            document.getElementById('input-jobids').value = jobIDs.join(' ');
        } catch (e) {
            console.log('Job ID data does not look right', jobIDs);
        }
    }

    function getTableMarkup(data) {
        var result = '';
        const names = data.requisition_name;
        const numCandidates = data.num_candidates;
        const numExports = data.num_exports;

        data.requisition_id.forEach(function(reqId, index) {
            result += `<tr>
              <td>${reqId}</td>
              <td>${names[index]}</td>
              <td class="data-number">${numCandidates[index]}</td>
              <td class="data-number">${numExports[index]}</td></tr>`;
        });
        return result;
    }

    function hasLocalData() {
        return (
            !!localStorage.getItem(LOCAL_STORAGE_ID_JOBIDS) &&
            getLocalJobIDs().length > 0
        );
    }

    function getLocalJobIDs() {
        try {
            return JSON.parse(localStorage.getItem(LOCAL_STORAGE_ID_JOBIDS));
        } catch (e) {
            console.log('Could not parse local job IDs');
            return [];
        }
    }

    function initTextArea() {
        if (hasLocalData()) {
            setTextArea(getLocalJobIDs());
        } else {
            getDefaultJobIDs(setTextArea);
        }
    }

    function initTable() {
        if (hasLocalData()) {
            getTableData(getLocalJobIDs());
        } else {
            getTableData();
        }
    }

    function clearLocalData() {
        localStorage.removeItem(LOCAL_STORAGE_ID_JOBIDS);
    }

    function resetDefaults() {
        clearLocalData();
        initData();
    }

    function getTableData(jobIDs) {
        jobIDs = jobIDs || '';
        if (Array.isArray(jobIDs)) {
            jobIDs = jobIDs.join(',');
        }
        $.ajax({
            url:
                '/data/direct-applications/wd-exports' +
                (jobIDs ? '/' + jobIDs : ''),
            success: function(data) {
                updateJobRequisitionData(data);
            },
            cache: false,
        });
    }

    function parseJobIDs(str) {
        var regExp = /([r|R]\d{7})/g,
            match,
            result = [];

        do {
            match = regExp.exec(str);
            if (match) {
                result.push(match[1]);
            }
        } while (match);
        return result;
    }

    function saveJobIDs(ids) {
        localStorage.setItem(LOCAL_STORAGE_ID_JOBIDS, JSON.stringify(ids));
    }

    function validateJobIDs(jobIDs) {
        if (!Array.isArray(jobIDs) || jobIDs.length === 0) {
            return [
                'Could not recognize any valid job ID. Please provide at least one.',
            ];
        }
        return [];
    }

    function onUpdateClick() {
        var jobIDs = parseJobIDs(document.getElementById('input-jobids').value);
        var errors = validateJobIDs(jobIDs);
        if (errors.length === 0) {
            saveJobIDs(jobIDs);
            getTableData(jobIDs);
        } else {
            alert(errors.join('\n'));
        }
    }

    function initEvents() {
        document
            .getElementById('btnUpdate')
            .addEventListener('click', onUpdateClick);
        document
            .getElementById('btnReset')
            .addEventListener('click', resetDefaults);
    }

    function initData() {
        initTextArea();
        initTable();
    }

    function init() {
        initData();
        initEvents();
    }

    function getDefaultJobIDs(callback) {
        $.ajax({
            url: '/data/direct-applications/job-ids',
            success: function(data) {
                callback(data);
            },
            cache: false,
        });
    }

    init();
});
