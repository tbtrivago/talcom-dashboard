import * as shared from './shared';

document.addEventListener('DOMContentLoaded', function(event) {
    const charts = {};

    var lastMonth = new Date();
    lastMonth.setMonth(lastMonth.getMonth() - 1);

    const pickmeupOptions = {
        mode: 'range',
        position: 'right',
        hide_on_select: true,
        max: new Date(),
        format: 'd b Y',
        calendars: 2,
        date: [lastMonth, new Date()],
    };

    function getSignupData(from, to) {
        getData('/data/growth/signups', from, to, function(data) {
            charts['signups'].series[0].setData(zip(data.dates, data.totals));
            charts['signups'].series[1].setData(zip(data.dates, data.deltas));
        });
    }

    function getSignupCareerLevelData(from, to) {
        getData('/data/growth/signups-by-clevel', from, to, function(data) {
            charts['signups-clevel'].series[0].setData(data.num);
            charts['signups-clevel'].xAxis[0].setCategories(
                data.careerLevel || [],
            );
        });
    }

    function getSignupJobFamilyData(from, to) {
        getData('/data/growth/candidates-by-jf', from, to, function(data) {
            charts['candidates-by-jf'].series[0].setData(data.num_users);
            charts['candidates-by-jf'].xAxis[0].setCategories(
                data.job_family || [],
            );
        });
    }

    var dataFunctions = {
        'daterange-signups': getSignupData,
        'daterange-cl': getSignupCareerLevelData,
        'daterange-jf': getSignupJobFamilyData,
    };

    const zip = (arr1, arr2) => {
        return arr1.map(function(item, index) {
            return [item, arr2[index]];
        });
    };

    const initOneDatePicker = datePickerId => {
        var dr = document.getElementById(datePickerId);
        dr.addEventListener('focus', function(e) {
            this.blur();
        });
        pickmeup(dr, pickmeupOptions);
        dr.addEventListener('pickmeup-change', function(e) {
            if (e.detail.formatted_date[0] === e.detail.formatted_date[1]) {
                return;
            }

            dataFunctions[datePickerId](
                formatDate(e.detail.date[0]),
                formatDate(e.detail.date[1]),
            );
        });
    };

    const initDatePickers = () => {
        for (var datePickerId in dataFunctions) {
            if (dataFunctions.hasOwnProperty(datePickerId)) {
                initOneDatePicker(datePickerId);
            }
        }
    };

    initDatePickers();

    function leadingZero(num) {
        if (num < 10) {
            return '0' + num;
        }
        return num;
    }

    function formatDate(date) {
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return (
            year + '-' + leadingZero(monthIndex + 1) + '-' + leadingZero(day)
        );
    }

    function sanitizeDateSpan(from, to) {
        to = to || formatDate(new Date());
        if (!from) {
            from = new Date(to);
            from.setMonth(from.getMonth() - 1);
            from = formatDate(from);
        }
        return {
            from: from,
            to: to,
        };
    }

    function getData(endpoint, from, to, callback) {
        var sanitized = sanitizeDateSpan(from, to);
        $.ajax({
            url: endpoint + '/' + sanitized.from + '/' + sanitized.to,
            success: callback,
            cache: false,
        });
    }

    charts['signups'] = Highcharts.chart('chart-01', {
        chart: {
            events: {
                load: function() {
                    getSignupData();
                },
            },
        },
        title: {
            text: 'Signups per day and totals',
        },
        xAxis: {
            type: 'datetime',
            labels: {
                format: '{value:%Y-%m-%d}',
                rotation: 45,
                align: 'left',
            },
        },
        yAxis: [
            {
                // Primary yAxis
                title: {
                    text: 'Total',
                    style: {
                        color: Highcharts.getOptions().colors[0],
                    },
                },
            },
            {
                // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Delta',
                },
                opposite: true,
                tickInterval: 100,
                tickPixelInterval: 40,
            },
        ],
        series: [
            {
                name: 'Total count',
                type: 'line',
                yAxis: 0,
                data: [],
            },
            {
                name: 'New signups',
                type: 'column',
                yAxis: 1,
                data: [],
            },
        ],
    });

    charts['signups-clevel'] = Highcharts.chart('chart-clevel', {
        chart: {
            events: {
                load: function() {
                    getSignupCareerLevelData();
                },
            },
        },
        title: {
            text: 'Signups by career level',
        },
        xAxis: {
            categories: ['Waiting for data...'],
        },
        yAxis: [
            {
                // Primary yAxis
                title: {
                    text: 'Signups',
                    style: {
                        color: Highcharts.getOptions().colors[0],
                    },
                },
            },
        ],
        series: [
            {
                name: 'Signups',
                type: 'column',
                yAxis: 0,
                data: [],
            },
        ],
    });

    charts['candidates-by-jf'] = Highcharts.chart('candidates-by-jf', {
        chart: {
            events: {
                load: function() {
                    getSignupJobFamilyData();
                },
            },
        },
        title: {
            text: 'Candidates by job family',
        },
        xAxis: {
            categories: ['Waiting for data...'],
        },
        yAxis: [
            {
                // Primary yAxis
                title: {
                    text: 'Candidates',
                    style: {
                        color: Highcharts.getOptions().colors[0],
                    },
                },
            },
        ],
        series: [
            {
                name: 'Candidates',
                type: 'column',
                yAxis: 0,
                data: [],
            },
        ],
    });
});
