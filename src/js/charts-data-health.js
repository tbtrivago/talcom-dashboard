import * as shared from './shared';

document.addEventListener('DOMContentLoaded', function(event) {
    var charts = {};

    charts['utm-stats'] = Highcharts.chart('chart-01', {
        chart: {
            events: {
                load: function() {
                    $.ajax({
                        url: '/data/data-health/utm-stats',
                        success: function(data) {
                            charts['utm-stats'].series[0].setData(
                                data.absolute,
                            );
                            charts['utm-stats'].series[1].setData(data.percent);
                            charts['utm-stats'].xAxis[0].setCategories(
                                data.param_names || [],
                            );
                        },
                        cache: false,
                    });
                },
            },
        },
        title: {
            text: 'UTM parameters (all-time)',
        },
        xAxis: {
            categories: ['Waiting for data...'],
        },
        yAxis: [
            {
                // Primary yAxis
                title: {
                    text: 'Absolute count',
                    style: {
                        color: Highcharts.getOptions().colors[0],
                    },
                },
            },
            {
                // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Percentage',
                },
                opposite: true,
                max: 100,
            },
        ],
        series: [
            {
                name: 'Absolute count',
                type: 'column',
                yAxis: 0,
                data: [],
            },
            {
                name: 'Percentage',
                type: 'column',
                yAxis: 1,
                data: [],
            },
        ],
    });

    charts['utm-stats-2week'] = Highcharts.chart('chart-02', {
        chart: {
            events: {
                load: function() {
                    $.ajax({
                        url:
                            '/data/data-health/utm-stats/' +
                            encodeURIComponent('2 week'),
                        success: function(data) {
                            charts['utm-stats-2week'].series[0].setData(
                                data.absolute,
                            );
                            charts['utm-stats-2week'].series[1].setData(
                                data.percent,
                            );
                            charts['utm-stats-2week'].xAxis[0].setCategories(
                                data.param_names || [],
                            );
                        },
                        cache: false,
                    });
                },
            },
        },
        title: {
            text: 'UTM parameters (last 2 weeks)',
        },
        xAxis: {
            categories: ['Waiting for data...'],
        },
        yAxis: [
            {
                // Primary yAxis
                title: {
                    text: 'Absolute count',
                    style: {
                        color: Highcharts.getOptions().colors[0],
                    },
                },
            },
            {
                // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Percentage',
                },
                opposite: true,
                max: 100,
            },
        ],
        series: [
            {
                name: 'Absolute count',
                type: 'column',
                yAxis: 0,
                data: [],
            },
            {
                name: 'Percentage',
                type: 'column',
                yAxis: 1,
                data: [],
            },
        ],
    });

    charts['signups-by-source'] = Highcharts.chart('signups-by-source', {
        chart: {
            events: {
                load: function() {
                    $.ajax({
                        url: '/data/data-health/signups-by-source',
                        success: function(data) {
                            charts['signups-by-source'].series[0].setData(
                                data.num,
                            );
                            charts['signups-by-source'].xAxis[0].setCategories(
                                data.utm_source || [],
                            );
                        },
                        cache: false,
                    });
                },
            },
        },
        title: {
            text: 'Candidates by source (last 30 days)',
        },
        xAxis: {
            categories: ['Waiting for data...'],
        },
        yAxis: [
            {
                // Primary yAxis
                title: {
                    text: 'Candidates',
                    style: {
                        color: Highcharts.getOptions().colors[0],
                    },
                },
            },
        ],
        series: [
            {
                name: 'Candidates',
                type: 'column',
                yAxis: 0,
                data: [],
            },
        ],
    });
});
