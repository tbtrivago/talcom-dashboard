import * as shared from './shared';

document.addEventListener('DOMContentLoaded', function(event) {
    var charts = {};

    charts['jf-ratings'] = Highcharts.chart('chart-01', {
        chart: {
            events: {
                load: function() {
                    $.ajax({
                        url: '/data/candidate-quality/jf-ratings',
                        success: function(data) {
                            charts['jf-ratings'].series[0].setData(
                                data.numRatings,
                            );
                            charts['jf-ratings'].series[1].setData(
                                data.avgRatings,
                            );
                            charts['jf-ratings'].xAxis[0].setCategories(
                                data.jobFamilies || [],
                            );
                        },
                        cache: false,
                    });
                },
            },
        },
        title: {
            text: 'Job family rating statistics',
        },
        xAxis: {
            categories: ['Waiting for job family data...'],
        },
        yAxis: [
            {
                // Primary yAxis
                title: {
                    text: 'Number of candidates rated',
                    style: {
                        color: Highcharts.getOptions().colors[0],
                    },
                },
            },
            {
                // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Average rating',
                },
                opposite: true,
            },
        ],
        series: [
            {
                name: 'Number of ratings',
                type: 'column',
                yAxis: 0,
                data: [],
            },
            {
                name: 'Average rating',
                type: 'line',
                yAxis: 1,
                data: [],
            },
        ],
    });
});
