import * as shared from './shared';

document.addEventListener('DOMContentLoaded', function(event) {
    function createLineChart(options) {
        return Highcharts.chart(options.divId, {
            chart: {
                type: 'line',
                events: {
                    load: options.onLoad || undefined,
                },
            },
            title: {
                text: options.title,
            },
            xAxis: {
                type: 'datetime',
                labels: {
                    format: '{value:%Y-%m-%d}',
                    rotation: 45,
                    align: 'left',
                },
            },
            yAxis: {
                title: {
                    text: options.yLabel,
                },
            },
            series: [
                {
                    name: options.seriesLabel,
                    data: [],
                },
            ],
        });
    }

    var charts = {};

    charts['returning-users'] = createLineChart({
        divId: 'chart-01',
        onLoad: createGetDataFunction('returning-users'),
        title: 'Returning users',
        yLabel: '#Users',
        seriesLabel: 'Returning users',
    });

    charts['video-count'] = Highcharts.chart('chart-02', {
        chart: {
            type: 'column',
            events: {
                load: createGetDataFunction('video-count'),
            },
        },
        title: {
            text: '#Users that record x videos',
        },
        xAxis: {
            type: 'linear',
        },
        yAxis: {
            title: {
                text: '#Users',
            },
        },
        series: [
            {
                name: 'Number of users',
                data: [],
            },
        ],
    });

    function createGetDataFunction(endpoint) {
        return function() {
            $.ajax({
                url: '/data/engagement/' + endpoint,
                success: function(data) {
                    charts[endpoint].series[0].setData(data);
                },
                cache: false,
            });
        };
    }
});
