const data = require('./data'),
      charts = require('./charts');

const setupRouting = (app) => {
    app.use('/data', data);

    app.use('/charts', charts);
    app.use('/', charts);
};

module.exports = {
    setupRouting
};