const express = require('express'),
      controller = require('../../controllers/engagementController');

const router = express.Router();

router.get('/returning-users/:from?/:until?', controller.returningUsers);
router.get('/video-count', controller.videoCount);


module.exports = router;