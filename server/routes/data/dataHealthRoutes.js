const express = require('express'),
      controller = require('../../controllers/dataHealthController');

let router = express.Router();

router.get('/signups-by-source/:from?/:until?', controller.signupsBySource);
router.get('/utm-stats-total', controller.utmDataTotal);
router.get('/utm-stats/:interval?', controller.utmDataInterval);

module.exports = router;

