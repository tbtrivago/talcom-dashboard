const express = require('express'),
      controller = require('../../controllers/candidateQualityController');

let router = express.Router();

router.get('/jf-ratings', controller.jobFamilyRatings);

module.exports = router;