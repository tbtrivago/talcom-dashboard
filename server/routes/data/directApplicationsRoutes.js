const router = require('express').Router(),
      controller = require('../../controllers/directApplicationsController');

router.get('/wd-exports/:jobids?', controller.exportedToWD);
router.get('/job-ids', controller.getDirectApplicationJobIDs);

module.exports = router;