const express = require('express'),
      controller = require('../../controllers/growthController');

const router = express.Router();

router.get('/signups/:from?/:until?', controller.signups);
router.get('/signups-by-clevel/:from?/:until?', controller.signupsByCareerLevel);
router.get('/candidates-by-jf/:from?/:until?', controller.signupsByJobFamily);

module.exports = router;