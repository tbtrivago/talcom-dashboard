const router = require('express').Router(),
      candidateQuality = require('./candidateQualityRoutes'),
      dataHealth = require('./dataHealthRoutes'),
      engagement = require('./engagementRoutes'),
      growth     = require('./growthRoutes'),
      directApps = require('./directApplicationsRoutes');

router.use('/candidate-quality', candidateQuality);
router.use('/data-health', dataHealth);
router.use('/engagement', engagement);
router.use('/growth', growth);
router.use('/direct-applications', directApps);

module.exports = router;