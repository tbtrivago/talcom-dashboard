const router = require('express').Router(),
      chartsRoutes = require('./chartsRoutes');

router.use('/', chartsRoutes);

module.exports = router;