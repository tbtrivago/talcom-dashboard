const express = require('express'),
      controller = require('../../controllers/chartController');
const router = express.Router();

router.get("/growth", controller.growth);
router.get("/engagement", controller.engagement);
router.get("/quality", controller.quality);
router.get("/data-health", controller.dataHealth);
router.get("/direct-applications", controller.directApplications);

router.get("/", controller.growth);

module.exports = router;
