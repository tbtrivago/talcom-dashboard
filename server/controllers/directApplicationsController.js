const directAppJobIds = [
     'R0001187',
     'R0001065',
     'R0000604',
     'R0000598',
     'R0000833',
     'R0001126',
     'R0001107',
     'R0000559',
     'R0001077',
     'R0001163',
     'R0000556',
     'R0000946',
     'R0000839',
     'R0000895',
     'R0001178'
];

const getJobReqIds = ids => {
    if (!ids || typeof ids !== 'string') {
        return directAppJobIds;
    }
    return ids.split(',');
};

const exportedToWD = (req, res, next) => {
    const repo = require('../../repositories/exportedToWD');
    repo.getData(getJobReqIds(req.params.jobids))
    .then(values => {
        res.json(values);
    })
    .catch(err => {
        next(err);
    });
};

const getDirectApplicationJobIDs = (req, res) => {
    res.json(directAppJobIds);
};

module.exports = {
    exportedToWD,
    getDirectApplicationJobIDs
};