const returningUsers = (req, res, next) => {
    const repo = require('../../repositories/returningUsers');
    repo.getData(req.params.from, req.params.until)
    .then(values => {
        res.json(values);
    })
    .catch(err => {
        next(err);
    });
};

const videoCount = (req, res, next) => {
    const repo = require('../../repositories/videoCount');
    repo.getData().then((result) => {
        res.json(result);
    }).catch(err => {
        next(err);
    });
};

module.exports = {
    returningUsers,
    videoCount
};