const { validateInterval } = require('../../util/dateUtil');

const signupsBySource = (req, res, next) => {
    const repo = require('../../repositories/signupsBySource');
    repo.getData(req.params.from, req.params.until)
    .then(values => {
        res.json(values);
    })
    .catch(err => {
        next(err);
    });
};

const utmDataTotal = (req, res, next) => {
    const repo = require('../../repositories/utmPresent');
    repo.getData('2 week').then((result) => {
        res.json(result);
    }).catch(err => {
        next(err);
    });
};

const utmDataInterval = (req, res, next) => {
    const repo = require('../../repositories/utmPresent');
    repo.getData(validateInterval(decodeURIComponent(req.params.interval))).then((result) => {
        res.json(result);
    }).catch(err => {
        next(err);
    });
};

module.exports = {
    signupsBySource,
    utmDataTotal,
    utmDataInterval
};