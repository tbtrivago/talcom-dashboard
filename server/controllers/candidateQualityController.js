
const jobFamilyRatings = (req, res, next) => {
    const repo = require('../../repositories/jobFamilyRatings');
    repo.getData().then((result) => {
        res.json(result);
    }).catch(err => {
        next(err);
    });
};

module.exports = {
    jobFamilyRatings
};