

const signups = (req, res, next) => {
    const repo = require('../../repositories/signups');
    repo.getData(req.params.from, req.params.until)
    .then(values => {
        res.json(values);
    })
    // redisify(repo).execute().then((result) => {
    //    res.json(result);
    .catch(err => {
        next(err);
    });
};

const signupsByCareerLevel = (req, res, next) => {
    const repo = require('../../repositories/signupsByCareerLevel');
    repo.getData(req.params.from, req.params.until)
    .then(values => {
        res.json(values);
    })
    .catch(err => {
        next(err);
    });
}

const signupsByJobFamily = (req, res, next) => {
    const repo = require('../../repositories/candidatesByJobFamily');
    repo.getData(req.params.from, req.params.until)
    .then(values => {
        res.json(values);
    })
    .catch(err => {
        next(err);
    });
};

module.exports = {
    signups,
    signupsByCareerLevel,
    signupsByJobFamily
};