
const growth = (req, res, next) => {
    res.render("growth", {
      title: 'Talent Community Dashboard - Growth',
      activeGrowth: 'active'
    });
};

const engagement = (req, res, next) => {
    res.render("engagement", {
      title: "Talent Community Dashboard - Engagement",
      activeEngagement: 'active'
    });
};

const quality = (req, res, next) => {
    res.render("quality", {
      title: "Talent Community Dashboard - Candidate Quality",
      activeQuality: 'active'
    });
};

const dataHealth = (req, res, next) => {
    res.render("data-health", {
      title: "Talent Community Dashboard - Data Health",
      activeHealth: 'active'
    });
};

const directApplications = (req, res, next) => {
    res.render("direct-applications", {
      title: "Talent Community Dashboard - Direct applications",
      activeDirect: 'active'
    });
};

module.exports = {
    growth,
    engagement,
    quality,
    dataHealth,
    directApplications
};