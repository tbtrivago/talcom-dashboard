const dateStep = require('./dateStep');
const moment  = require('moment');

const DATE_FORMAT = 'YYYY-MM-DD';

const getDateFormatForStep = (step) => {
    switch(step) {
        case dateStep.MONTH: return '%Y-%m';
        case dateStep.YEAR:  return '%Y';
        default: return '%Y-%m-%d';
    }
};

/**
 * 
 * @param {string} from 
 * @param {string} until 
 */
const getValidDateSpan = (from, until) => {
    // If "until" date not given, use current date
    if (!until || !isValidDate(until)) {
        until = moment().format(DATE_FORMAT);
    }
    // If "from" date not given or invalid, subtract 30 days from "until" date
    if (!from || !isValidDate(from) || !moment(from).isBefore(until)) {
        from = moment(until).subtract(30, 'days').format(DATE_FORMAT);
    } 
    return {
        from, 
        until
    };
};

/**
 * 
 * @param {string} from 
 * @param {string} until 
 */
const determineDates = (from, until) => {
    ({ from, until } = getValidDateSpan(from, until));
    const result = [];
    let fromObj = moment(from);
    while (fromObj.isBefore(until)) {
        result.push(fromObj.format(DATE_FORMAT));
        fromObj = fromObj.add(1, 'days');
    }
    result.push(until);
    return result;
};

const isValidDate = (date) => {
    return (typeof date === 'string' && date.match(/\d{4}-\d{2}-\d{2}/));
};

const isDateInterval = interval => {
    return ['day', 'week', 'month', 'year'].indexOf(interval) >= 0;
};

const validateInterval = interval => {
    const parts = interval.split(' ');
    if (!parts[0].match(/\d+/) || !isDateInterval(parts[1])) {
        return '100 year';
    } 
    return parts[0] + ' ' + parts[1];
};

module.exports = {
    getDateFormatForStep,
    determineDates,
    getValidDateSpan,
    validateInterval
};


