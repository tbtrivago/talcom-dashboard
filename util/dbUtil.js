

const getFirstValue = (dbResult, fieldName) => {
    for (const item of dbResult) {
        return item[fieldName];
    }
    return undefined;
};

const dbRowToArray = (row, fieldNames) => {
    const result = [];
    for (let name of fieldNames) {
        result.push(row[name]);
    }
    return result;
};

/**
 * Returns an array of arrays. Each contained array
 * represents one record in the DB result. The items
 * will be in the order of the field names you pass
 * in.
 * 
 * Example:
 * const f = dbResultToArray('shape', 'color');
 * f(dbResult) // Might yield [['round', 'red'], ['triangular', 'yellow']]
 * 
 * const g = dbResultToArray('color', 'shape');
 * g(dbResult) // Might yield [['red', 'round'], ['yellow', 'triangular']]
 * 
 * @param {} fieldNames 
 */
const dbResultToArray = (...fieldNames) => (dbResult) => {
    let data = [];
    dbResult.forEach((item) => {
        data.push(dbRowToArray(item, fieldNames));
    });
    return data;
};


/**
 * Returns an object with fieldNames as keys to 
 * value arrays.
 * 
 * @param {string} fieldNames 
 */
const dbResultToObject = (...fieldNames) => (dbResult) => {
    const result = {};
    fieldNames.forEach(item => {
        result[item] = [];
    });
    dbResult.forEach(row => {
        fieldNames.forEach(fieldName => {
            result[fieldName].push(row[fieldName]);
        });
    });

    return result;
};

module.exports = {
    getFirstValue,
    dbResultToArray,
    dbResultToObject
};
