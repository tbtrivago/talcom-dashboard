const path = require('path');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: {
        'data-health': __dirname + '/src/js/charts-data-health.js',
        'direct-app': __dirname + '/src/js/charts-direct-applications.js',
        engagement: __dirname + '/src/js/charts-engagement.js',
        growth: __dirname + '/src/js/charts-growth.js',
        quality: __dirname + '/src/js/charts-quality.js',
        shared: __dirname + '/src/js/shared.js',
    },
    output: {
        path: __dirname + '/public/javascripts', // Folder to store generated bundle
        filename: '[name].bundle.js', // Name of generated bundle after build
        chunkFilename: '[name].bundle.js',
        publicPath: '/', // public URL of the output directory when referenced in a browser
    },
    module: {
        // where we defined file patterns and their loaders
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: [/node_modules/],
            },
            {
                test: /\.html$/,
                use: 'html-loader',
                exclude: path.join(__dirname, 'views/*'),
            },
        ],
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                sourceMap: true,
                uglifyOptions: {
                    mangle: false,
                },
            }),
        ],
    },
    plugins: [
        // Array of plugins to apply to build chunk
        // new HtmlWebpackPlugin({
        //     template: __dirname + '/templates/growth.html',
        //     filename: 'growth.html',
        //     inject: 'body',
        //     chunks: ['shared', 'growth'],
        // }),
        // new HtmlWebpackPlugin({
        //     template: __dirname + '/templates/engagement.html',
        //     filename: 'engagement.html',
        //     inject: 'body',
        //     chunks: ['shared', 'engagement'],
        // }),
    ],
    devServer: {
        contentBase: './public', //source of static assets
        port: 7700,
    },
};
