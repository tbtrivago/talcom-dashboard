FROM node:9.11 
LABEL Maintainer="Thomas Bartel <thomas.bartel@trivago.com>"

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install --only=production

COPY . .

EXPOSE 3000

CMD ["yarn", "start", "--host", "0.0.0.0"]