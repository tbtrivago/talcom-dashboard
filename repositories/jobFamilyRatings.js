const connect = require('./connect');

const fieldNameFamilyName = 'name';
const fieldNameNumRatings = 'num_ratings';
const fieldNameAvgRating  = 'avg_rating';

const sql = `
select jf.name, count(jfr.id) as num_ratings, round(avg(jfr.rating), 2) as avg_rating
  from job_family_rating jfr
 inner join job_family jf
    on jfr.job_family_id = jf.id
 where jfr.rating is not null
   and jfr.relationship_type = 3
 group by jf.id
 order by jf.id`;

const transformDbResult = (dbResult) => {
    let ratingsData = {
        jobFamilies: [],
        numRatings: [],
        avgRatings: []
    };
    dbResult.forEach((item) => {
        ratingsData.jobFamilies.push(item[fieldNameFamilyName]);
        ratingsData.numRatings.push(item[fieldNameNumRatings]);
        ratingsData.avgRatings.push(item[fieldNameAvgRating]);
    });
    return ratingsData;
};

const getKeys = () => {
    return ['jobfamratings'];
};

/**
 * Returns a promise that resolves with job family data:
 * {
 *     'jobFamilies': ['Analytics', 'Media & Marketing', ...],
 *     'numRatings': [200, 532, ...],
 *     'avgRatings': [2.00, 2.76, ...]
 * }
 */
const getData = () => {
    return connect.talsearch()
        .then(con => con.query(sql))
        .then(transformDbResult);
};


module.exports = {
    getData,
    getRepoName: () => 'jobFamilyRatingsRepo',
    getExpirationSeconds: () => 12 * 3600,
    getKeys
};
