const connect = require('./connect');
const sprintf = require('sprintf-js').sprintf;
const { getValidDateSpan } = require('../util/dateUtil');
const { dbResultToArray, dbResultToObject }  = require('../util/dbUtil');

const fieldNameNum    = 'num_users';
const fieldNameFamily = 'job_family';

// qc.question_id: 124 = expertise, 123 = interested in
const sql = `
select qc.group as ${fieldNameFamily}, count(uq.user_id) as ${fieldNameNum}
  from question_choice qc
 inner join question q on q.id = qc.question_id
 inner join user_questionnaire_answer uqa on q.id = uqa.question_id
 inner join user_questionnaire_answer_question_choice uqaqc 
         on uqa.id = uqaqc.user_questionnaire_answer_id
		and qc.id = uqaqc.question_choice_id
 inner join user_questionnaire uq on uqa.user_questionnaire_id = uq.id
 inner join user u on uq.user_id = u.id
 where qc.question_id = 124
   and u.created_at >= '%1$s'
   and u.created_at <  '%2$s'
 group by qc.group
 order by qc.group;
`;

const getQuery = (from, until) => {
    return sprintf(sql, from, until);
};

const getKeys = (from, until) => {
    ({ from, until } = getValidDateSpan(from, until));
    return [from + '--' + until];
};

const processDBResult = dbResultToObject(fieldNameFamily, fieldNameNum);

const getData = (from, until) => {
    ({ from, until } = getValidDateSpan(from, until));
    return connect.talcom()
    .then(con => con.query(getQuery(from, until)))
    .then(processDBResult);
};


module.exports = {
    getData,
    getRepoName: () => 'candidatesByJobFamilyRepo',
    getExpirationSeconds: () => 12 * 3600,
    getKeys
};