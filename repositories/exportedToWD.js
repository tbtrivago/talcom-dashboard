const connect = require('./connect');
const sprintf = require('sprintf-js').sprintf;
const { dbResultToObject } = require('../util/dbUtil');

const fieldNameRequisitionId   = 'requisition_id';
const fieldNameRequisitionName = 'requisition_name';
const fieldNameNumExports = 'num_exports';
const fieldNameNumCandidates = 'num_candidates';

const sqlCandidates = `
select jp.requisition_id, count(c.id) as ${fieldNameNumCandidates}
   from job_position jp
   left outer join candidate c
     on jp.id = c.interested_job_id
  where requisition_id in (%1$s)
  group by requisition_id
  order by field(requisition_id, %1$s)
`;

const sqlExports = `
select jp.requisition_id, jp.name as requisition_name,
       count(c2.id) as num_exports
  from job_position jp
  left outer join (
  
    select c.id, c.interested_job_id 
    from candidate c
	 inner join  (
     
    select candidate_id, min(created_at) as min_date
      from workday_event
         where status = 'EXPORTED'
         group by candidate_id
         
         ) wde
    on c.id = wde.candidate_id
    ) c2
    
    on jp.id = c2.interested_job_id
 where requisition_id in (%1$s)
 group by requisition_id
 order by field(requisition_id, %1$s)
 `;

const getKeys = () => {
    return ['exported_to_wd'];
};

const encloseInQuotes = str => "'" + str + "'";

const validateJobReqIds = jobReqIds => {
    if (!Array.isArray(jobReqIds) || jobReqIds.length === 0) {
        jobReqIds = [''];
    }
    return jobReqIds;
};

const injectJobReqIds = (jobReqIds, statement) => {
    jobReqIds = validateJobReqIds(jobReqIds);
    const quotedJobReqIds = jobReqIds.map(encloseInQuotes).join(',');
    return sprintf(statement, quotedJobReqIds);
};

const getExportsSql = jobReqIds => injectJobReqIds(jobReqIds, sqlExports);
const getCandidatesSql = jobReqIds => injectJobReqIds(jobReqIds, sqlCandidates);

const transformExportDbResult = dbResultToObject(fieldNameRequisitionId, fieldNameRequisitionName, fieldNameNumExports);
const transformCandidatesDbResult = dbResultToObject(fieldNameRequisitionId, fieldNameNumCandidates);

const combineDBResults = ([dbResultExports, dbResultCandidates]) => { 
    const resultExports = transformExportDbResult(dbResultExports);
    const resultCandidates = transformCandidatesDbResult(dbResultCandidates);
    return {
        ...resultExports,
        [fieldNameNumCandidates]: resultCandidates[fieldNameNumCandidates]
    };    
};

const getData = (jobReqIds) => {
    return connect.talsearch()
        .then(con => {
            return Promise.all([
                con.query(getExportsSql(jobReqIds)),
                con.query(getCandidatesSql(jobReqIds))
            ]);
        })
        .then(combineDBResults);
};

module.exports = {
    getData,
    getRepoName: () => 'exportedToWdRepo',
    getExpirationSeconds: () => 12 * 3600,
    getKeys
};
