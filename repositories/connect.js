module.exports.talcom = () => {
    return require('promise-mysql').createConnection({
        host: process.env.TALCOM_MYSQL_HOST,
        user: process.env.TALCOM_MYSQL_USER,
        password: process.env.TALCOM_MYSQL_PASSWORD,
        database: process.env.TALCOM_MYSQL_DATABASE
      });
};

module.exports.talsearch = () => {
    return require('promise-mysql').createConnection({
        host: process.env.TALSEARCH_MYSQL_HOST,
        user: process.env.TALSEARCH_MYSQL_USER,
        password: process.env.TALSEARCH_MYSQL_PASSWORD,
        database: process.env.TALSEARCH_MYSQL_DATABASE
      });
};