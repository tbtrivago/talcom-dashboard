

const defaultExpirationSeconds = 12 * 3600;
const redisTimeout = 2000;

const repoInterface = {
    repoName: 'string',
    expirationSeconds: 3600,
    getData: (params) => {

    }
};

module.exports = (repo) => {

    const saveToRedis = data => {
        const client = require('./redisConn').client;
        client.set(repo.repoName, JSON.stringify(data), 
                   'EX', repo.expirationSeconds || defaultExpirationSeconds);
    };

    return {
        getRepoName: () => repo.getRepoName(),

        getExpirationSeconds: () => repo.getExpirationSeconds(),

        getKeys: (...params) => {
            return repo.getKeys(...params);
        },

        getData: (...params) => {
            const redis = require('./redisConn');
            let promisePending = true;
        
            return new Promise((resolve, reject) => {
                // Assume Redis is unavailable if this hits
                const timeout = setTimeout(() => {
                    promisePending = false;
                    resolve(repo.getData(...params));
                }, redisTimeout);
        
                // Reject on Redis error
                redis.client.on('error', (err) => {
                    clearTimeout(timeout);
                    if (promisePending) {
                        promisePending = false;
                        resolve(repo.getData(...params));
                    }
                });

                // const allKeys = repo.getKeys(...params);
                // redis.client.multiGet(allKeys, (err, values) => {
                //     clearTimeout(timeout);
                //     promisePending = false;
                //     if (err) {
                //         reject(err);
                //     } else {
                //         // Find out which keys are missing
                //         // For missing keys, get only those from repo
                //         // When repo result available, save the missing keys and return combined value
                //     }
                // });
        
                redis.client.get(repo.getRepoName, (err, value) => {
                    clearTimeout(timeout);
                    promisePending = false;
                    if (err) {
                        reject(err);
                    } else {
                        value = JSON.parse(value);
                        if (!value) {
                            resolve(repo.getData().then(result => {
                                saveToRedis(result);
                                return result;
                            }));
                        } else {
                            resolve(value);
                        }
                    }
                });
        
            });
        }
    };
};