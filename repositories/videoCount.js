const connect = require('./connect');
const { dbResultToArray }  = require('../util/dbUtil');

const fieldNameVideos = 'num_videos';
const fieldNameUsers  = 'num_users';

const sql = `SELECT 
    ${fieldNameVideos},
    count(id) as ${fieldNameUsers}
FROM 
	(SELECT u.id, sum(if(isnull(uqa.text), 0, 1)) as num_videos
       FROM user u
       LEFT JOIN user_questionnaire uq on u.id = uq.user_id
       LEFT JOIN user_questionnaire_answer uqa on uq.id = uqa.user_questionnaire_id
       LEFT JOIN question q on uqa.question_id = q.id
	  WHERE q.id in (65,66,67,68,69)
      GROUP BY u.id) num_videos_by_user
GROUP BY num_videos_by_user.num_videos`;

const transformDbResult = dbResultToArray(fieldNameVideos, fieldNameUsers);

const getKeys = () => {
    return ['numVideos'];
};

/**
 * Returns a promise that resolves with video data:
 * [
 *   [0, 16534],
 *   [1, 2520],
 *   [2, 93],
 *   [3, 154],
 *   [4, 242],
 *   [5, 1523],
 * ]
 */
const getData = () => {
    return connect.talcom()
        .then(con => con.query(sql))
        .then(transformDbResult);
};

module.exports = {
    getData,
    getRepoName: () => 'videoCountRepo',
    getExpirationSeconds: () => 12 * 3600,
    getKeys
};

