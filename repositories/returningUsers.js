const { dbResultToArray } = require('../util/dbUtil');

const connect  = require('./connect');
const sprintf  = require('sprintf-js').sprintf;
const moment   = require('moment');
const dateStep = require('../util/dateStep');
const { determineDates, getDateFormatForStep, getValidDateSpan } = require('../util/dateUtil');

const fieldNameNumUsers = 'RETURNING_USERS';
const fieldNameDate     = 'RETURN_DATE';

const returningUsersSql = `
    select DATE_FORMAT(last_login, '%1$s') as ${fieldNameDate}, count(id) as ${fieldNameNumUsers}
      from user
     where last_login > created_at
       and last_login >= %2$s
       and last_login <= %3$s
     group by ${fieldNameDate}
`;

const getReturningUsersSql = (from, until, step = dateStep.DAY) => {
    until = until || from;
    if (until === from) {
        until = `DATE_ADD('${from}', INTERVAL 1 ${step})`;
    } else {
        until = `'${until}'`;
    }
    from = "'" + from + "'";
    return sprintf(returningUsersSql, getDateFormatForStep(step), from, until);
};

const getKeys = (from, until, step = dateStep.DAY) => {
    ({ from, until } = getValidDateSpan(from, until));
    let dates = determineDates(from, until);
    return dates.map((item) => step + '.' + item);
};

const getData = (from, until, step = dateStep.DAY) => {
    ({ from, until } = getValidDateSpan(from, until));
    return connect.talcom()
            .then(con => con.query(getReturningUsersSql(from, until, step)))
            .then(dbResultToArray(fieldNameDate, fieldNameNumUsers))
            .then(values => values.map(arr => {
                return [moment(arr[0]).valueOf(), arr[1]];
            }));
};

module.exports = {
    getData,
    getRepoName: () => 'returningUsersRepo',
    getExpirationSeconds: () => 12 * 3600,
    getKeys
};


