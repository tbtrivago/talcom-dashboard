const connect = require('./connect');
const { getFirstValue, dbResultToArray } = require('../util/dbUtil');
const sprintf =  require('sprintf-js').sprintf;
const moment  =  require('moment');
const dateStep = require('../util/dateStep');
const { determineDates, getDateFormatForStep } = require('../util/dateUtil');

const fieldNameTotal   = 'TOTAL_USERS';
const fieldNameSignups = 'SIGNUPS';
const fieldNameDate    = 'SIGNUP_DATE';

const totalUsersSql = `
    select count(id) as ${fieldNameTotal}
      from user 
     where created_at < %s
`;

const signupsSql = `
    select DATE_FORMAT(created_at, '%1$s') as ${fieldNameDate}, count(id) as ${fieldNameSignups}
      from user
     where DATE_FORMAT(created_at, '%1$s') in (%2$s)
     group by ${fieldNameDate}
`;

const getTotalSignupsSql = (date) => {
    if (typeof date !== 'string' || !date.match(/\d{4}-\d{2}-\d{2}/)) {
        date = 'NOW()';
    } else {
        date = "'" + date + "'";
    }
    return sprintf(totalUsersSql, date);
};

const getSignupsDeltasSql = (step = dateStep.DAY, dates) => {
    const datesString = dates.map(item => "'" + item + "'").join(',');
    return sprintf(signupsSql, getDateFormatForStep(step), datesString);
};

// Function that takes a DB result
const deltaResultToArray = dbResultToArray(fieldNameDate, fieldNameSignups);

const combineDBResults = ([dbResultInitial, dbResultDeltas]) => {
    let initial = parseInt(getFirstValue(dbResultInitial, fieldNameTotal));
    let values = deltaResultToArray(dbResultDeltas);

    let result = {
        dates: [],
        deltas: [],
        totals: []
    };

    let current = initial;
    let counter = 0;
    for (let [date, num] of values) {
        num = parseInt(num);
        result.dates.push(moment(date).valueOf());
        result.totals.push(current += num);
        result.deltas.push(num);
    }

    return result;
};

const getKeys = (from, until, step = dateStep.DAY) => {
    let dates = determineDates(from, until);
    return dates.map((item) => step + '.' + item);
};

const getData = (from, until, step = dateStep.DAY) => {
        let dates = determineDates(from, until);
        return connect.talcom()
        .then(con => {
            return Promise.all([
                con.query(getTotalSignupsSql(dates[0])),
                con.query(getSignupsDeltasSql(step, dates))
            ]);
        })
        .then(combineDBResults);
};

module.exports = {
    getData,
    getRepoName: () => 'signupsRepo',
    getExpirationSeconds: () => 12 * 3600,
    getKeys
};
