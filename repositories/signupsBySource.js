const connect = require('./connect');
const sprintf = require('sprintf-js').sprintf;
const { getValidDateSpan } = require('../util/dateUtil');
const { dbResultToArray, dbResultToObject }  = require('../util/dbUtil');

const fieldNameNum   = 'num';
const fieldNameValue = 'utm_source';

const sql = `SELECT ${fieldNameValue}, COUNT(id) AS ${fieldNameNum} 
    FROM candidate
    WHERE (created_at BETWEEN '%1$s' AND '%2$s') 
    AND ${fieldNameValue} <> ''
    GROUP BY ${fieldNameValue}
    ORDER BY ${fieldNameNum}
    DESC LIMIT 10`;

const getQuery = (from, until) => {
    return sprintf(sql, from, until);
};

const getKeys = (from, until) => {
    ({ from, until } = getValidDateSpan(from, until));
    return [from + '--' + until];
};

const processDBResult = dbResultToObject(fieldNameValue, fieldNameNum);

const getData = (from, until) => {
    ({ from, until } = getValidDateSpan(from, until));
    return connect.talsearch()
    .then(con => con.query(getQuery(from, until)))
    .then(processDBResult);
};


module.exports = {
    getData,
    getRepoName: () => 'signupsBySource',
    getExpirationSeconds: () => 12 * 3600,
    getKeys
};
