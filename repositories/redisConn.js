const redis = require('redis');
const { promisify } = require('util');

const client = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT    
});

client.on("error", (err) => {
    console.log("Redis error: " + err);
});

module.exports = {
    client: client,
    get: promisify(client.get).bind(client),
    onError: ((f) => {
        client.on('error', f);
    })
};