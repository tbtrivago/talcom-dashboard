const connect = require('./connect');
const sprintf = require('sprintf-js').sprintf;
const { getValidDateSpan } = require('../util/dateUtil');
const { dbResultToArray, dbResultToObject }  = require('../util/dbUtil');

const fieldNameNum   = 'num';
const fieldNameValue = 'careerLevel';

const sql = `select qc.value as ${fieldNameValue}, 
            count(uqaqc.user_questionnaire_answer_id) as ${fieldNameNum}
  from question_choice qc
 inner join user_questionnaire_answer_question_choice uqaqc on qc.id = uqaqc.question_choice_id
 inner join user_questionnaire_answer uqa on uqaqc.user_questionnaire_answer_id = uqa.id
 inner join user_questionnaire uq on uqa.user_questionnaire_id = uq.id
 inner join user u on uq.user_id = u.id
 where qc.question_id = 15
   and u.created_at >= '%1$s'
   and u.created_at <  '%2$s'
 group by qc.value
 order by qc.id`;

const getQuery = (from, until) => {
    return sprintf(sql, from, until);
};

const getKeys = (from, until) => {
    ({ from, until } = getValidDateSpan(from, until));
    return [from + '--' + until];
};

const processDBResult = dbResultToObject(fieldNameValue, fieldNameNum);

const getData = (from, until) => {
    ({ from, until } = getValidDateSpan(from, until));
    return connect.talcom()
    .then(con => con.query(getQuery(from, until)))
    .then(processDBResult);
};


module.exports = {
    getData,
    getRepoName: () => 'signupsByCareerLevelRepo',
    getExpirationSeconds: () => 12 * 3600,
    getKeys
};
