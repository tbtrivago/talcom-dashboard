const connect = require('./connect');
const { getFirstValue } = require('../util/dbUtil');

const sprintf = require('sprintf-js').sprintf;

const utmParams = ['utm_campaign', 'utm_source', 'utm_medium', 'utm_content'];

const utmGivenSql = 
    "select count(id) as NUM, '%1$s' as param_name \
       from user \
      where %1$s is not null \
        and %1$s <> '' \
        and created_at > date_sub(NOW(), interval %2$s);"

const totalUsersSql = "select count(*) as num from user \
                        where created_at > date_sub(NOW(), interval %1$s);";

const getKeys = () => {
    return ['utmPresentData'];
};

// First one is total count, then utm_values
const combineDBResults = (results) => {
    let totalUsers = parseInt(getFirstValue(results[0], 'num'));
    let result = {
        param_names: [],
        absolute: [],
        percent: []
    };

    for (let i = 1; i < results.length; i++) {
        let utmCount = parseInt(getFirstValue(results[i], 'NUM'));
        let utmPercentage = Math.round(utmCount * 10000 / totalUsers) / 100;
        result.param_names.push(getFirstValue(results[i], 'param_name'));
        result.absolute.push(utmCount);
        result.percent.push(utmPercentage);
    }

    return result;
};

/**
 * 
 * @param {*} interval If blank, '100 year' is used. Could be "2 week", or "1 month", etc
 */
const getData = (interval = '100 year') => {
    return connect.talcom()
        .then(con => {
            const utmPromises = utmParams.map(param_name => {
                return con.query(sprintf(utmGivenSql, param_name, interval));
            });
            return Promise.all([
                con.query(sprintf(totalUsersSql, interval)),
                ...utmPromises
            ]);
        })
        .then(combineDBResults);
};

module.exports = {
    getData,
    getRepoName: () => "utmPresentRepo",
    getExpirationSeconds: () => 12 * 3600,
    getKeys
};